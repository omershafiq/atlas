package com.varian.services

import com.varian.auth.BraidAuthProvider
import com.varian.flows.WhoAmIFlow
import com.varian.flows.rules.*

import io.bluebank.braid.corda.BraidConfig
import io.vertx.core.http.HttpServerOptions
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken

/**
 * A regular Corda service that bootstraps the Braid server when the node
 * starts.
 *
 * The Braid server offers a user-defined set of flows and services.
 *
 * @property serviceHub the node's `AppServiceHub`.
 */
@CordaService
class BootstrapBraidService(val serviceHub: AppServiceHub) : SingletonSerializeAsToken() {

    init {
        BraidConfig.fromResource(configFileName)?.bootstrap()
    }

    private fun BraidConfig.bootstrap() {
        this.withFlow(WhoAmIFlow::class)


                .withFlow(CreateUserFlow::class)
                .withFlow(CreatePrescriptionFlow::class)
                .withFlow(ProposeTreatmentPlanFlow::class)
                .withFlow(RequestUpdateTreatmentPlanFlow::class)
                .withFlow(AcceptTreatmentPlanFlow::class)


                .withFlow(GetPrescriptionFlow::class)
                .withFlow(GetAllTreatmentPlanProposalsFlow::class)
                .withFlow(GetAllTreatmentPlansFlow::class)
                .withFlow(GetAllTreatmentPlanUnderReviewFlow::class)
                .withFlow(GetAllApprovedTreatmentPlanFlow::class)

                .withFlow(GetUsersFlow::class)




                .withService("myService", BraidService(serviceHub))
                .withAuthConstructor { BraidAuthProvider(serviceHub.myInfo.legalIdentities.first()) }
                .withHttpServerOptions(HttpServerOptions().setSsl(false))
                .bootstrapBraid(serviceHub)
    }

    /**
     * config file name based on the node legal identity
     */
    private val configFileName: String
        get() {
            val name = serviceHub.myInfo.legalIdentities.first().name.organisation
            return "braid-$name.json"
        }
}