package com.varian.auth

import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.AbstractUser
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.User
import net.corda.core.identity.Party
import org.apache.shiro.io.ResourceUtils
import java.util.*

/*

Authentication for this cordapp is limited due to specific use-case, a JWT based security approach can also be implemented in this case.
However, the client side will be secured with JWT and since the owner of the client app and cordapp are same its secure to have password base access for PoC.
(In case of production this will be replaced by an RSA based authentication or JWT)

*/

class BraidAuthProvider : AuthProvider {

  private var party: Party? = null

  constructor(node: Party) {
     party = node
  }

  override fun authenticate(authInfo: JsonObject, callback: Handler<AsyncResult<User>>) {
    try {
        val username = authInfo.getString("username") ?: throw RuntimeException("no username found")
        val password = authInfo.getString("password") ?: throw RuntimeException("no password found")
        callback.handle(Future.succeededFuture(Authenticate(username, password, party)))
    } catch (err: Throwable) {
        callback.handle(Future.failedFuture(err.message))
    }
  }
}

class Authenticate(private val userName: String, private val password: String, party: Party?) : AbstractUser() {

  private val properties: Properties = Properties()
  private val stream = ResourceUtils.getInputStreamForPath("classpath:shiro.properties")

  init {
          properties.load(stream)
          val userInfo = properties.getProperty("user." + party!!.name.organisation.toLowerCase())
          val userPassword = userInfo.split(',')[0]
          val username = userInfo.split(',')[1]
          val userOrganization = userInfo.split(',')[2]

          if(userName != username || userPassword != password || party.name.organisation != userOrganization) {
            throw Exception("Invalid username or password!")
          }
  }

  override fun doIsPermitted(permission: String, callback: Handler<AsyncResult<Boolean>>) {
      callback.handle(Future.succeededFuture(true))
  }

  override fun principal(): JsonObject {
    val result = JsonObject()
    result.put("username", userName)
    return result
  }

  override fun setAuthProvider(provider: AuthProvider) {
  }
}
