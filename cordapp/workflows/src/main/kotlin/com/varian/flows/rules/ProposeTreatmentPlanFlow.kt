package com.varian.flows.rules

import co.paralleluniverse.fibers.Suspendable
import com.google.gson.Gson
import com.varian.contracts.TreatmentPlanContract
import com.varian.states.TreatmentPlanState
import com.varian.utils.getNodeTime
import com.varian.utils.getPartyByOrganizationName
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey
import java.util.*
import kotlin.collections.ArrayList


@InitiatingFlow
@StartableByService
class ProposeTreatmentPlanFlow(val proposedBy: String, val proposedDoctors: String, val proposedPhysicist: String, var treatmentPlanData: String, val notes: String) : FlowLogic<MutableMap<String, Any>>() {
    @Suspendable
    override fun call() : MutableMap<String, Any> {
        return try {

            val gson = Gson()
            val proposedDoctorMapLiteral = gson.fromJson(proposedDoctors, Map::class.java)
            val proposedDoctorMap = proposedDoctorMapLiteral as Map<String, String>

            val proposedPhysicistMapLiteral = gson.fromJson(proposedPhysicist, Map::class.java)
            val proposedPhysicistMap = proposedPhysicistMapLiteral as Map<String, String>


            if(treatmentPlanData == "") {

                val tPlans = listOf(
                    "JSu-IM102",
                    "JSu-IM105",
                    "JSu-IM107")

                val random = Random()
                val response = khttp.get("http://junction-planreview.azurewebsites.net/api/patients/Lung/plans/"+ tPlans[random.nextInt(3)])
                if(response.statusCode == 200) {
                    treatmentPlanData = response.text
                } else {
                    println("cannot call api")
                }

            }

            val participants = mutableListOf<Party>()

            //add doctors
            proposedDoctorMap.forEach {
                participants.add(  getPartyByOrganizationName(it.value, serviceHub) )
            }

            //add physicists
            proposedPhysicistMap.forEach {
                participants.add(  getPartyByOrganizationName(it.value, serviceHub) )
            }

            participants.add(ourIdentity)

            val finalParticipantsList = participants.toSet().toList()

            // Creating the output.
            val output = TreatmentPlanState(proposedBy, ourIdentity, proposedDoctorMap, proposedPhysicistMap, getNodeTime(), treatmentPlanData,
                    finalParticipantsList, "{}", "PROPOSED", "NA", notes)


            // Creating the command.
            val commandType = TreatmentPlanContract.Commands.Propose()

            val mutableRequiredSigners: MutableList<PublicKey> = mutableListOf()

            //Add counterparty agencies keys
            finalParticipantsList.forEach {
               mutableRequiredSigners.add(it.owningKey)
            }

            //Add our key
            mutableRequiredSigners.add(ourIdentity.owningKey)
            val requiredSigners = mutableRequiredSigners.toSet().toList() //Transforming to 'Set' will remove redundancies if any
            val command = Command(commandType, requiredSigners)


            // Building the transaction.
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val txBuilder = TransactionBuilder(notary)
            txBuilder.addOutputState(output, TreatmentPlanContract.ID)
            txBuilder.addCommand(command)


            // Verify Contract
            txBuilder.verify(serviceHub)


            // Signing the transaction ourselves.
            val partStx = serviceHub.signInitialTransaction(txBuilder)


            // Gathering the counterparties signature.
            val counterpartiesMutableList: MutableList<Party>
            counterpartiesMutableList = finalParticipantsList.toMutableList()

            val counterpartiesSet = counterpartiesMutableList.toSet()

            val sessionList: ArrayList<FlowSession> = ArrayList()
            for (counterparty in counterpartiesSet) {
                if (counterparty != ourIdentity) {
                    sessionList.add(initiateFlow(counterparty))
                }
            }

            val fullyStx = subFlow(CollectSignaturesFlow(partStx, sessionList))

            // Finalising the transaction.
            val finalisedTx = subFlow(FinalityFlow(fullyStx, sessionList))
            val outputState = finalisedTx.tx.outputsOfType<TreatmentPlanState>().single()

            mutableMapOf("status" to "OK", "output" to outputState)
        } catch (ex: Exception) {
            mutableMapOf("status" to "error", "message" to ex.localizedMessage)
        }
    }
}

@InitiatedBy(ProposeTreatmentPlanFlow::class)
class ResponderProposeTreatmentPlanFlow(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) {
                val ledgerTx = stx.toLedgerTransaction(serviceHub, false)

//                if (!getOncologists(serviceHub).contains(ourIdentity)) {
//                    throw FlowException("Only an oncologist can receive review plan.")
//                }
            }
        }

        val txId = subFlow(signTransactionFlow).id
        subFlow(ReceiveFinalityFlow(counterpartySession, txId))
    }
}
