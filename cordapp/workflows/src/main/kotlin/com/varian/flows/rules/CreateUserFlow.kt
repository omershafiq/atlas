package com.varian.flows.rules

import co.paralleluniverse.fibers.Suspendable
import com.varian.contracts.UserContract
import com.varian.states.UserState
import com.varian.utils.getPartyByOrganizationName
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey


@InitiatingFlow
@StartableByService
class CreateUserFlow(private val name: String,
                             private val type: String) : FlowLogic<MutableMap<String, Any>>() {
    @Suspendable
    override fun call() : MutableMap<String, Any> {
        return try {

            val counterParty: Party
            if(ourIdentity.name.organisation == "HUS"){
                counterParty = getPartyByOrganizationName("Terveystalo", serviceHub)
            }
            else {
                counterParty = getPartyByOrganizationName("HUS", serviceHub)
            }

            // Creating the output.
            val output = UserState(name, type, listOf(ourIdentity, counterParty), ourIdentity.name.organisation)
            println(listOf(ourIdentity, counterParty))


            // Creating the command.
            val commandType = UserContract.Commands.Create()

            val mutableRequiredSigners: MutableList<PublicKey> = mutableListOf()

            //Add counterparty agencies keys
            mutableRequiredSigners.add(counterParty.owningKey)


            //Add our key
            mutableRequiredSigners.add(ourIdentity.owningKey)
            val requiredSigners = mutableRequiredSigners.toSet().toList() //Transforming to 'Set' will remove redundancies if any
            val command = Command(commandType, requiredSigners)


            // Building the transaction.
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val txBuilder = TransactionBuilder(notary)
            txBuilder.addOutputState(output, UserContract.ID)
            txBuilder.addCommand(command)


            // Verify Contract
            txBuilder.verify(serviceHub)


            // Signing the transaction ourselves.
            val partStx = serviceHub.signInitialTransaction(txBuilder)



            // Gathering the counterparties signature.
            val counterpartiesMutableList: MutableList<Party> = mutableListOf(counterParty)

            val counterpartiesSet = counterpartiesMutableList.toSet()

            val sessionList: ArrayList<FlowSession> = ArrayList()
            for (counterparty in counterpartiesSet) {
                if (counterparty != ourIdentity) {
                    sessionList.add(initiateFlow(counterparty))
                }
            }

            val fullyStx = subFlow(CollectSignaturesFlow(partStx, sessionList))

            // Finalising the transaction.
            val finalisedTx = subFlow(FinalityFlow(fullyStx, sessionList))
            val outputState = finalisedTx.tx.outputsOfType<UserState>().single()

            mutableMapOf("status" to "OK", "output" to outputState)
        } catch (ex: Exception) {
            mutableMapOf("status" to "error", "message" to ex.localizedMessage)
        }
    }
}

@InitiatedBy(CreateUserFlow::class)
class ResponderCreateUserFlow(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) {
                val ledgerTx = stx.toLedgerTransaction(serviceHub, false)
            }
        }

        val txId = subFlow(signTransactionFlow).id
        subFlow(ReceiveFinalityFlow(counterpartySession, txId))
    }
}
