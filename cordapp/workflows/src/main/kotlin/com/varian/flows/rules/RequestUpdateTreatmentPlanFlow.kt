package com.varian.flows.rules

import co.paralleluniverse.fibers.Suspendable
import com.google.gson.Gson
import com.varian.contracts.TreatmentPlanContract
import com.varian.states.TreatmentPlanState
import com.varian.utils.getNodeTime
import com.varian.utils.getPartyByOrganizationName
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey


@InitiatingFlow
@StartableByService
class RequestUpdateTreatmentPlanFlow(private val proposedPlanId: String, private val updateProposer: String, private val updatedTreatmentPlan: String, val notes: String) : FlowLogic<MutableMap<String, Any>>() {
    @Suspendable
    override fun call() : MutableMap<String, Any> {
        return try {


            // Retrieving the input from the vault.
            val inputCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(UniqueIdentifier.fromString(proposedPlanId)))
            val inputStateAndRef = serviceHub.vaultService.queryBy<TreatmentPlanState>(inputCriteria).states.single()

            val input = inputStateAndRef.state.data

            val output = input.copy(input.proposedBy, input.proposedByParty, input.proposedDoctors, input.proposedPhysicist,
                    getNodeTime(), updatedTreatmentPlan, input.participantsNodes,
                    input.acceptanceDigitalSignatures, "UPDATE", updateProposer, notes, input.linearId)

            val participants = mutableListOf<Party>()

            output.participantsNodes.forEach {
                participants.add(it)
            }


            val finalParticipantsList = participants.toSet().toList()

            // Creating the command.
            val commandType = TreatmentPlanContract.Commands.Update()

            val mutableRequiredSigners: MutableList<PublicKey> = mutableListOf()

            //Add counterparty agencies keys
            finalParticipantsList.forEach {
                mutableRequiredSigners.add(it.owningKey)
            }

            //Add our key
            mutableRequiredSigners.add(ourIdentity.owningKey)
            val requiredSigners = mutableRequiredSigners.toSet().toList() //Transforming to 'Set' will remove redundancies if any
            val command = Command(commandType, requiredSigners)


            // Building the transaction.
            val notary = inputStateAndRef.state.notary
            val txBuilder = TransactionBuilder(notary)
            txBuilder.addInputState(inputStateAndRef)
            txBuilder.addOutputState(output, TreatmentPlanContract.ID)
            txBuilder.addCommand(command)

            // Verify Contract
            txBuilder.verify(serviceHub)

            // Signing the transaction ourselves.
            val partStx = serviceHub.signInitialTransaction(txBuilder)

            // Gathering the counterparty's signature.
            val counterpartiesMutableList: MutableList<Party>
            counterpartiesMutableList = finalParticipantsList.toMutableList()

            val counterpartiesSet = counterpartiesMutableList.toSet()

            val sessionList: ArrayList<FlowSession> = ArrayList()
            for (counterparty in counterpartiesSet) {
                if (counterparty != ourIdentity) {
                    sessionList.add(initiateFlow(counterparty))
                }
            }

            val fullyStx = subFlow(CollectSignaturesFlow(partStx, sessionList))

            // Finalising the transaction.
            val finalisedTx = subFlow(FinalityFlow(fullyStx, sessionList))
            val outputState = finalisedTx.tx.outputsOfType<TreatmentPlanState>().single()

            mutableMapOf("status" to "OK", "output" to outputState)
        } catch (ex: Exception) {
            mutableMapOf("status" to "error", "message" to ex.localizedMessage)
        }
    }
}

@InitiatedBy(RequestUpdateTreatmentPlanFlow::class)
class ResponderRequestUpdateTreatmentPlanFlow(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) {
                val ledgerTx = stx.toLedgerTransaction(serviceHub, false)


//                if (!getPhysicians(serviceHub).contains(ourIdentity)) {
//                    throw FlowException("Only physician can accept updates.")
//                }
            }
        }

        val txId = subFlow(signTransactionFlow).id

        subFlow(ReceiveFinalityFlow(counterpartySession, txId))
    }
}