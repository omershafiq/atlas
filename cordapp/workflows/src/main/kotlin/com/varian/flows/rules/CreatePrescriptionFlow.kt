package com.varian.flows.rules

import co.paralleluniverse.fibers.Suspendable
import com.varian.contracts.PrescriptionContract
import com.varian.states.PrescriptionState
import com.varian.utils.getNodeTime
import com.varian.utils.getPartyByOrganizationName
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey


@InitiatingFlow
@StartableByService
class CreatePrescriptionFlow(private val issuedTo: String,
                             private val issuedToParty: String,
                             private val issuedBy: String,
                             private val data: String) : FlowLogic<MutableMap<String, Any>>() {
    @Suspendable
    override fun call() : MutableMap<String, Any> {
        return try {

            // Creating the output.
            val output = PrescriptionState(issuedTo, getPartyByOrganizationName(issuedToParty, serviceHub),
                    issuedBy, ourIdentity, getNodeTime(), data)


            // Creating the command.
            val commandType = PrescriptionContract.Commands.Create()

            val mutableRequiredSigners: MutableList<PublicKey> = mutableListOf()

            //Add our key
            mutableRequiredSigners.add(ourIdentity.owningKey)
            val requiredSigners = mutableRequiredSigners.toSet().toList() //Transforming to 'Set' will remove redundancies if any
            val command = Command(commandType, requiredSigners)


            // Building the transaction.
            val notary = serviceHub.networkMapCache.notaryIdentities.first()
            val txBuilder = TransactionBuilder(notary)
            txBuilder.addOutputState(output, PrescriptionContract.ID)
            txBuilder.addCommand(command)


            // Verify Contract
            txBuilder.verify(serviceHub)


            // Signing the transaction ourselves.
            val partStx = serviceHub.signInitialTransaction(txBuilder)


            // Gathering the counterparties signature.
            val counterpartiesMutableList: MutableList<Party> = mutableListOf(getPartyByOrganizationName(issuedToParty, serviceHub))

            val counterpartiesSet = counterpartiesMutableList.toSet()

            val sessionList: ArrayList<FlowSession> = ArrayList()
            for (counterparty in counterpartiesSet) {
                if (counterparty != ourIdentity) {
                    sessionList.add(initiateFlow(counterparty))
                }
            }

            val fullyStx = subFlow(CollectSignaturesFlow(partStx, sessionList))

            // Finalising the transaction.
            val finalisedTx = subFlow(FinalityFlow(fullyStx, sessionList))
            val outputState = finalisedTx.tx.outputsOfType<PrescriptionState>().single()

            mutableMapOf("status" to "OK", "output" to outputState)
        } catch (ex: Exception) {
            mutableMapOf("status" to "error", "message" to ex.localizedMessage)
        }
    }
}

@InitiatedBy(CreatePrescriptionFlow::class)
class ResponderCreatePrescriptionFlow(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) {
                val ledgerTx = stx.toLedgerTransaction(serviceHub, false)
            }
        }

        val txId = subFlow(signTransactionFlow).id
        subFlow(ReceiveFinalityFlow(counterpartySession, txId))
    }
}
