package com.varian.flows.rules

import co.paralleluniverse.fibers.Suspendable
import com.varian.states.TreatmentPlanState
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByService
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria


@InitiatingFlow
@StartableByService
class GetAllTreatmentPlanProposalsFlow() : FlowLogic<MutableMap<String, Any>>() {
    @Suspendable
    override fun call() : MutableMap<String, Any> {
        return try {
            val unconsumedCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
            val unconsumed = serviceHub.vaultService.queryBy<TreatmentPlanState>(unconsumedCriteria).states.filter {
                it.state.data.status == "PROPOSED"
            }
            mutableMapOf("status" to "OK", "output" to unconsumed)
        } catch (ex: Exception) {
            mutableMapOf("status" to "error", "message" to ex.localizedMessage)
        }
    }
}