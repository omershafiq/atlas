package com.varian.flows.rules

import co.paralleluniverse.fibers.Suspendable
import com.varian.states.PrescriptionState
import com.varian.states.TreatmentPlanState
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByService
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria


@InitiatingFlow
@StartableByService
class GetAllTreatmentPlansFlow() : FlowLogic<MutableMap<String, Any>>() {
    @Suspendable
    override fun call() : MutableMap<String, Any> {
        return try {
            val consumedCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.CONSUMED)
            val unconsumedCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
            val consumed = serviceHub.vaultService.queryBy<TreatmentPlanState>(consumedCriteria).states
            val unconsumed = serviceHub.vaultService.queryBy<TreatmentPlanState>(unconsumedCriteria).states
            mutableMapOf("status" to "OK", "old" to consumed, "latest" to unconsumed)
        } catch (ex: Exception) {
            mutableMapOf("status" to "error", "message" to ex.localizedMessage)
        }
    }
}