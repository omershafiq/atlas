package com.varian.utils

import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import java.time.LocalDateTime

fun getNodeByOrganizationName(nodeName: String, serviceHub: ServiceHub): CordaX500Name {
    val networkMap = serviceHub.networkMapCache
    for (nodeInfo in networkMap.allNodes){
        val nodeNamePlain = nodeInfo.legalIdentities.first().name.organisation
        if(nodeName.equals(nodeNamePlain, ignoreCase = true)) {
            return nodeInfo.legalIdentities.first().name
        }
    }
    throw IllegalArgumentException("Node name not recognized")
}

fun getPartyByOrganizationName(nodeName: String, serviceHub: ServiceHub): Party {
    val networkMap = serviceHub.networkMapCache
    for (nodeInfo in networkMap.allNodes){
        val nodeNamePlain = nodeInfo.legalIdentities.first().name.organisation
        if(nodeName.equals(nodeNamePlain, ignoreCase = true)) {
            return nodeInfo.legalIdentities.first()
        }
    }
    throw IllegalArgumentException("Node name not recognized")
}


fun getNodeTime(): String {
    //TODO: Can be replaced by an oracle that gets time from internet clock or this function can be changed to get time from internet clocks
    return LocalDateTime.now().toString()
}