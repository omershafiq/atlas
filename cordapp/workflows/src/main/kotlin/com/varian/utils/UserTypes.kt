package com.varian.utils

enum class UserTypes(val user: String) {
        PATIENT("Patient"),
        DOCTOR("Doctor"),
        PLANNER("Planner"),
        PHYSICIST("Physicist")
}