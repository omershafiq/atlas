package com.varian.utils

enum class DocumentTypes(val type: String) {
    REQUEST("Request"),
    VALIDATION("Validation"),
    APPROVED("Approved"),
}