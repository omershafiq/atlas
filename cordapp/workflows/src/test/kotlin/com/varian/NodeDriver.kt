package com.varian

import net.corda.core.identity.CordaX500Name
import net.corda.core.node.NetworkParameters
import net.corda.core.utilities.getOrThrow
import net.corda.testing.common.internal.testNetworkParameters
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.driver
import net.corda.testing.node.TestCordapp
import net.corda.testing.node.User

/**
 * Allows you to run your nodes through an IDE (as opposed to using deployNodes). Do not use in a production
 * environment.
 */
fun main(args: Array<String>) {
    val rpcUsers = listOf(User("user1", "test", permissions = setOf("ALL")))
    val nParams = testNetworkParameters(minimumPlatformVersion = 4)
    driver(DriverParameters(
            isDebug = true,
            startNodesInProcess = true,
            waitForAllNodesToFinish = true,
            extraCordappPackagesToScan = listOf("io.cordite.dgl", "io.cordite.dgl.contract.v1", "io.cordite.commons.distribution"),
            networkParameters = nParams)
            ) {
        startNode(providedName = CordaX500Name("HUS", "Helsinki", "FI"), rpcUsers = rpcUsers).getOrThrow()
        startNode(providedName = CordaX500Name("Terveystalo", "Turku", "FI"), rpcUsers = rpcUsers).getOrThrow()
//        startNode(providedName = CordaX500Name("Varian", "Espoo", "FI"), rpcUsers = rpcUsers).getOrThrow()
    }
}
