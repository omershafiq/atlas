"use strict";

const express = require('express')
const path = require('path')
const cors = require('cors')
const Proxy = require('braid-client').Proxy;
var bodyParser = require('body-parser')


const app = express()
app.use(cors())

// Connects to Braid running on the node.
let braid = new Proxy({
  url: "http://localhost:8080/api/", credentials: {
  username: 'hus', password: '3WlpZnZ8$nyV' }
}, onOpen, onClose, onError, { strictSSL: false });

function onOpen() { console.log('Connected to HUS node.'); }
function onClose() { console.log('Disconnected from HUS node.'); }
function onError(err) { console.error(err); process.exit(); }

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static(__dirname))

app.get('/mesh', function(req,res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.sendfile(path.join(__dirname + '/index.html'));
});

// Uses Braid to call the WhoAmI flow on the node, and handles the response
// using callbacks.
app.get('/whoami-flow-callback', (req, res) => {
    braid.flows.whoAmIFlow(
        result => res.send("Hey, you're speaking to " + result + "!"),
        err => res.status(500).send(err));
});

// Uses Braid to call the WhoAmI flow on the node, and handles the response
// using promises.
app.get('/whoami-flow-promise', (req, res) => {
    braid.flows.whoAmIFlow()
    .then(result => res.send("Hey, you're speaking to " + result + "!"))
    .catch(err => res.status(500).send(err));    
});

// Uses Braid to call the BraidService on the node, and handles the response
// using callbacks.
app.get('/whoami', (req, res) => {
    braid.myService.whoAmI(
        result => res.send({"status": "OK",
         "node": result}),
        err => res.status(500).send(err));
});


app.post('/create-user', (req, res) => {
    braid.flows.createUserFlow(req.body.name, req.body.type)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

app.post('/create-prescription', (req, res) => {
    braid.flows.createPrescriptionFlow(req.body.issuedTo, req.body.issuedToParty, req.body.issuedBy, req.body.data)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

app.post('/propose-tp', (req, res) => {
    braid.flows.proposeTreatmentPlanFlow(req.body.proposedBy, req.body.proposedDoctors, req.body.proposedPhysicist, req.body.treatmentPlanData, req.body.notes)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

app.post('/update-tp', (req, res) => {
    braid.flows.requestUpdateTreatmentPlanFlow(req.body.proposedPlanId, req.body.updateProposer, req.body.updatedTreatmentPlan, req.body.notes)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

app.post('/accept-tp', (req, res) => {
    braid.flows.acceptTreatmentPlanFlow(req.body.planId, req.body.acceptor, req.body.notes)
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});




//GET CALLS
app.get('/get-prescriptions', (req, res) => {
    braid.flows.getPrescriptionFlow()
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});

app.get('/get-treatment-plans', (req, res) => {
       braid.flows.getAllTreatmentPlansFlow()
       .then(result => res.send(result))
       .catch(err => res.status(500).send(err));
   });

app.get('/get-users', (req, res) => {
    braid.flows.getUsersFlow()
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err));
});





app.listen(3001, () => console.log('HUS Server listening on port 3001!'))