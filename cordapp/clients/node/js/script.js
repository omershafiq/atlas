var meshdemo = {
    scene: null,
    camera: null,
    renderer: null,
    container: null,
    controls: null,
    clock: null,
    stats: null,
    init: function () { // Initialization
        // create main scene
        this.scene = new THREE.Scene();
        this.scene.fog = new THREE.FogExp2(0xffffff, 0.0003);
        var SCREEN_WIDTH = window.innerWidth,
            SCREEN_HEIGHT = window.innerHeight;
        // prepare camera
        var VIEW_ANGLE = 44, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
        this.camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
        this.scene.add(this.camera);
        this.camera.position.set(30, -400, 200);
        // prepare renderer
        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        this.renderer.setClearColor(this.scene.fog.color);
        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapSoft = true;
        // prepare container
        this.container = document.createElement('div');
        document.body.appendChild(this.container);
        this.container.appendChild(this.renderer.domElement);
        // events
        THREEx.WindowResize(this.renderer, this.camera);
        // prepare controls (OrbitControls)
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.target = new THREE.Vector3(0, 0, 0);
        this.controls.maxDistance = 3000;
        // prepare clock
        this.clock = new THREE.Clock();
        // prepare stats
        this.stats = new Stats();
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.left = '50px';
        this.stats.domElement.style.top = '50px';
        this.stats.domElement.style.zIndex = 1;
        this.container.appendChild(this.stats.domElement);
        this.scene.add(new THREE.AmbientLight(0x606060));
        // light
        var dirLight = new THREE.DirectionalLight(0xffffff);
        dirLight.position.set(200, 200, 1000).normalize();
        this.camera.add(dirLight);
        this.camera.add(dirLight.target);
        // load models
        this.loadModels();
    },
    loadModels: function () {
        // prepare PLY loader and load the model
        var oPlyLoader = new THREE.PLYLoader();
        var baseUrl = window.location.origin;

        var meshUrl = 'https://junction-planreview.azurewebsites.net/api/patients/Lung/images/1622-Series-CT01/structure-meshes/Heart';
        var meshUrl_b = 'https://junction-planreview.azurewebsites.net/api/patients/Lung/images/1622-Series-CT01/structure-meshes/Body';
        var meshUrl_x = 'https://junction-planreview.azurewebsites.net/api/patients/Lung/plans/JSu-IM102/isodose-meshes/31.500-Gy';
        var meshUrl_y = 'https://junction-planreview.azurewebsites.net/api/patients/Lung/images/1622-Series-CT01/structure-meshes/PTV_63';

        var meshUrl_lr = 'https://junction-planreview.azurewebsites.net/api/patients/Lung/images/1622-Series-CT01/structure-meshes/Lung-right';
        var meshUrl_ll = 'https://junction-planreview.azurewebsites.net/api/patients/Lung/images/1622-Series-CT01/structure-meshes/Lung-left';

        oPlyLoader.load(meshUrl, function (geometry) {
            geometry.computeVertexNormals();
            var material = new THREE.MeshPhongMaterial({ color: 0xff0000, specular: 0x111111 });
            material.side = THREE.DoubleSide;
            var mesh = new THREE.Mesh(geometry, material);
            mesh.position.set(0, 0, 0);
            meshdemo.scene.add(mesh);
        });

//        oPlyLoader.load(meshUrl_b, function (geometry) {
//                    geometry.computeVertexNormals();
//                    var material = new THREE.MeshPhongMaterial({ color: 0x0055ff, specular: 0x111111 });
//                    material.side = THREE.DoubleSide;
//                    var mesh = new THREE.Mesh(geometry, material);
//                    mesh.position.set(0, 0, 0);
//                    meshdemo.scene.add(mesh);
//                });

//         oPlyLoader.load(meshUrl_x, function (geometry) {
//                            geometry.computeVertexNormals();
//                            var material = new THREE.MeshPhongMaterial({ color: 0x00ff00, specular: 0x111111 });
//                            material.side = THREE.DoubleSide;
//                            var mesh = new THREE.Mesh(geometry, material);
//                            mesh.position.set(0, 0, 0);
//                            meshdemo.scene.add(mesh);
//                        });

         oPlyLoader.load(meshUrl_lr, function (geometry) {
                            geometry.computeVertexNormals();
                            var material = new THREE.MeshPhongMaterial({ color: 0xFFC0CB, specular: 0x111111 });
                            material.side = THREE.DoubleSide;
                            var mesh = new THREE.Mesh(geometry, material);
                            mesh.position.set(0, 0, 0);
                            meshdemo.scene.add(mesh);
                        });

        oPlyLoader.load(meshUrl_ll, function (geometry) {
                                      geometry.computeVertexNormals();
                                      var material = new THREE.MeshPhongMaterial({ color: 0xFFC0CB, specular: 0x111111 });
                                      material.side = THREE.DoubleSide;
                                      var mesh = new THREE.Mesh(geometry, material);
                                      mesh.position.set(0, 0, 0);
                                      meshdemo.scene.add(mesh);
                                  });
    }
};

// Animate the scene
function animate() {
    requestAnimationFrame(animate);
    render();
    update();
}
// Update controls and stats
function update() {
    meshdemo.controls.update(meshdemo.clock.getDelta());
    meshdemo.stats.update();
}
// Render the scene
function render() {
    if (meshdemo.renderer) {
        meshdemo.renderer.render(meshdemo.scene, meshdemo.camera);
    }
}
// Initialize lesson on page load
function initializeLesson() {
    meshdemo.init();
    animate();
}
if (window.addEventListener)
    window.addEventListener('load', initializeLesson, false);
else if (window.attachEvent)
    window.attachEvent('onload', initializeLesson);
else window.onload = initializeLess