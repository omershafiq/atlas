const fetch = require('node-fetch');

var HUS_URL = "http://localhost:3001"
var TERVYSTALO_URL = "http://localhost:3002"

function createUser(node, name, type) {

var url = ""
if(node === "HUS"){
url = HUS_URL
}
else if(node === "Terveystalo") {
url = TERVYSTALO_URL
}

var headers = {
  "Content-Type": "application/json",
}
var payload = {
  "name": name,
  "type": type
}
fetch(url+'/create-user', { method: 'POST', headers: headers, body: JSON.stringify(payload)})
  .then((res) => {
     return res.json()
})
.then((json) => {
  console.log(json);
  // Do something with the returned data.
});
}


//Create Patients
createUser("HUS", "Lung", "Patient")

//Create Doctors
createUser("HUS", "Paul", "Doctor")
createUser("HUS", "Tony", "Doctor")

//Create Physicist
createUser("HUS", "Alex", "Physicist")

//Create Planner
createUser("Terveystalo", "John", "Planner")


createUser("HUS", "Jason", "Patient")
createUser("HUS", "Peter", "Patient")
createUser("HUS", "Linda", "Patient")
createUser("HUS", "Jessica", "Patient")





