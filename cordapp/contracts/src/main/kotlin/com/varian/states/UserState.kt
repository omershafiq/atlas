package com.varian.states

import com.varian.contracts.UserContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party

@BelongsToContract(UserContract::class)
data class UserState(
        val name: String,
        val type: String,
        val parties: List<Party>,
        val node: String,
        override val linearId: UniqueIdentifier = UniqueIdentifier()) : LinearState {

    /** The public keys of the involved parties. */
    override val participants: List<AbstractParty> get() = parties
}
