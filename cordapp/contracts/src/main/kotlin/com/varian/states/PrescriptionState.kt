package com.varian.states

import com.varian.contracts.PrescriptionContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party

@BelongsToContract(PrescriptionContract::class)
data class PrescriptionState(
        val issuedTo: String,
        val issuedToParty: Party,
        val issuedBy: String,
        val issuedByParty: Party,
        val issueDateTime: String,
        val data: String,
        override val linearId: UniqueIdentifier = UniqueIdentifier()) : LinearState {

    /** The public keys of the involved parties. */
    override val participants: List<AbstractParty> get() = listOf(issuedToParty, issuedByParty)
}
