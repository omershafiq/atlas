package com.varian.states


import com.varian.contracts.TreatmentPlanContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party

@BelongsToContract(TreatmentPlanContract::class)
data class TreatmentPlanState(
        val proposedBy: String,
        val proposedByParty: Party,
        val proposedDoctors: Map<String, String>,
        val proposedPhysicist: Map<String, String>,
//        val proposedDoctors: List<String>,
//        val proposedDoctorParties: List<Party>,
//        val proposedPhysicist: List<String>,
//        val proposedPhysicistParties: List<Party>,
        val proposalDateTime: String,
        val treatmentPlanData: String,
        val participantsNodes: List<Party>,
        val acceptanceDigitalSignatures: String,
        val status: String,
        val updateProposer: String,
        val notes: String,
        override val linearId: UniqueIdentifier = UniqueIdentifier()) : LinearState {

    /** The public keys of the involved parties. */
    override val participants: List<AbstractParty> get() = participantsNodes
}
