package com.varian.contracts

import com.varian.states.PrescriptionState
import com.varian.states.TreatmentPlanState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction

// ************
// * Contract *
// ************
class TreatmentPlanContract : Contract {
    companion object {
        // Used to identify our contract when building a transaction.
        const val ID = "com.varian.contracts.TreatmentPlanContract"
    }

    // A transaction is valid if the verify() function of the contract of all the transaction's input and output states
    // does not throw an exception.
    override fun verify(tx: LedgerTransaction) {
        val cmd = tx.commands.requireSingleCommand<Commands>()

        when (cmd.value) {
            is Commands.Propose -> requireThat {
                "Creating prescription doesn't allow inputs" using (tx.inputStates.isEmpty())
                "There is exactly one output" using (tx.outputStates.size == 1)
                "The single output is of type PrescriptionState" using (tx.outputsOfType<TreatmentPlanState>().size == 1)
                "There is exactly one command" using (tx.commands.size == 1)
                "There is no timestamp" using (tx.timeWindow == null)

                val output = tx.outputsOfType<TreatmentPlanState>().single()

                  //TODO: Define rules


                output.participantsNodes.forEach {
                    "All involved participants are required signers" using (cmd.signers.contains(it.owningKey))
                }

            }

            is Commands.Update -> requireThat {

                "There is exactly one input" using (tx.inputStates.size == 1)
                "The single input is of type TokenTypeRuleSetState" using (tx.inputsOfType<TreatmentPlanState>().size == 1)
                "There is exactly one output" using (tx.outputStates.size == 1)
                "The single output is of type TokenTypeRuleSetState" using (tx.outputsOfType<TreatmentPlanState>().size == 1)
                "There is exactly one command" using (tx.commands.size == 1)
                "There is no timestamp" using (tx.timeWindow == null)

                val output = tx.outputsOfType<TreatmentPlanState>().single()
                val input = tx.inputsOfType<TreatmentPlanState>().single()


                //TODO: Define rules

                output.participantsNodes.forEach {
                    "All involved participants are required signers" using (cmd.signers.contains(it.owningKey))
                }
            }

            is Commands.Approve -> requireThat {

                "There is exactly one input" using (tx.inputStates.size == 1)
                "The single input is of type TokenTypeRuleSetState" using (tx.inputsOfType<TreatmentPlanState>().size == 1)
                "There is exactly one output" using (tx.outputStates.size == 1)
                "The single output is of type TokenTypeRuleSetState" using (tx.outputsOfType<TreatmentPlanState>().size == 1)
                "There is exactly one command" using (tx.commands.size == 1)
                "There is no timestamp" using (tx.timeWindow == null)

                val output = tx.outputsOfType<TreatmentPlanState>().single()
                val input = tx.inputsOfType<TreatmentPlanState>().single()


                //TODO: Define rules

                output.participantsNodes.forEach {
                    "All involved participants are required signers" using (cmd.signers.contains(it.owningKey))
                }
            }
        }
    }

    // Used to indicate the transaction's intent.
    interface Commands : CommandData {
        class Propose : Commands
        class Update : Commands
        class Approve : Commands
    }
}