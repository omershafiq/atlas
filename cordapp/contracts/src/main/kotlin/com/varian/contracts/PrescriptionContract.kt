package com.varian.contracts

import com.varian.states.PrescriptionState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction

// ************
// * Contract *
// ************
class PrescriptionContract : Contract {
    companion object {
        // Used to identify our contract when building a transaction.
        const val ID = "com.varian.contracts.PrescriptionContract"
    }

    // A transaction is valid if the verify() function of the contract of all the transaction's input and output states
    // does not throw an exception.
    override fun verify(tx: LedgerTransaction) {
        val cmd = tx.commands.requireSingleCommand<Commands>()

        when (cmd.value) {
            is Commands.Create -> requireThat {
                "Creating prescription doesn't allow inputs" using (tx.inputStates.isEmpty())
                "There is exactly one output" using (tx.outputStates.size == 1)
                "The single output is of type PrescriptionState" using (tx.outputsOfType<PrescriptionState>().size == 1)
                "There is exactly one command" using (tx.commands.size == 1)
                "There is no timestamp" using (tx.timeWindow == null)

                val output = tx.outputsOfType<PrescriptionState>().single()

                  //TODO: Define rules

                "The issuedBy is a required signer" using (cmd.signers.contains(output.issuedByParty.owningKey))
                "The issuedTo is a required signer" using (cmd.signers.contains(output.issuedByParty.owningKey))
            }

            //TODO: Update prescription
        }
    }

    // Used to indicate the transaction's intent.
    interface Commands : CommandData {
        class Create : Commands
    }
}