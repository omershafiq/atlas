# Project Atlas

This project used Corda Blockchain (http://www.corda.net) to solve varian junction challange.

React-Native bases iOS iPad application source code can be found here: https://github.com/max-prokopenko/junction2019/

# Description:

Our team Atlas has build state of the art decentralised system using blockchain technology which helps doctors, planners and physicist to communicate efficiently with each other through all the phases of treatment plan for a specific patient during radiation therapy. Cryptographic digital signatures are being used for the consensus of doctors and physicist to approve or disapprove certain treatment plan. We are providing interactive, easy to use mobile interface having 3d models which will help allowed personals to review and generarte treatment plans more efficiently.

Stakeholders of the system have the ability to trace the time line of treatment plan being generated which gives audit trail. Our system has the potential to transform the auditing process into a quick, efficient, and cost-effective procedure, and bring more trust and integrity to the data businesses use and share every day.



# Junction Challange:

With increased life expectancy and an aging population, the global burden to provide cancer treatment increases in the future. The planning of radiation therapy (RT) consists of optimizing the arrangement of ionizing radiation sources in order to obtain enough therapeutic radiation to the defined target region while avoiding too much radiation to critical organs and normal tissue. Once a radiation therapy treatment plan has been created, it needs to be approved by the physician. The plan review takes a lot of time from one of the critical resources (oncologist) in the clinic and we are looking for novel solutions utilizing modern technology to reduce the spend in that.

Once RT plan has been created by the planner (dosimetrist or physicist), it is stored in a database, waiting for the physicist to approve it, which requires (among other things)
- to go to the workstation to view the plan, potentially together with the planner.
- to check congruence to the treatment protocol, i.e. reviewing a plethora of constraints related to the dose distributions, quality of radiation source positioning, volume definitions, etc.
- if changes to the plan are needed, to communicate with the planner – often verbally or with side-notes.
